import express from "express";
import cors from "cors";
import axios from "axios";
import weatherDao from "./weather-dao-mongoose";

const apiRouter = express.Router();
const apiRoute = express.Router();

var PersistentDeviseModel = weatherDao.ThisPersistentModel;
var app = express();

apiRouter.use(
  cors({
    origin: ["http://localhost:4200"], //'*',
    methods: ["GET", "POST", "DELETE", "UPDATE", "PUT"],
  })
);

function statusCodeFromEx(ex) {
  let status = 500;
  let error = ex ? ex.error : null;
  switch (error) {
    case "BAD_REQUEST":
      status = 400;
      break;
    case "NOT_FOUND":
      status = 404;
      break;
    //...
    case "CONFLICT":
      status = 409;
      break;
    default:
      status = 500;
  }
  return status;
}


apiRouter

  .route("/weather-api/weather/:zip")
  .get(async function (req, res, next) {
    var zip = req.params.zip;
    try {
      let weather = await weatherDao.findById(zip);
      res.header("Access-Control-Allow-Origin", "*");
      res.send(weather);
    } catch (ex) {
      res.status(statusCodeFromEx(ex)).send(ex);
    }
  });

apiRoute.route("/weather-api/weathers").get(async function (req, res, next) {
  try {
    let weathers = await weatherDao.findAll();
    res.header("Access-Control-Allow-Origin", "*");
    res.send(weathers);
  } catch (ex) {
    res.status(statusCodeFromEx(ex)).send(ex);
  }
});

export default { apiRouter, apiRoute };
// CORS enabled with express/node-js :
